// Naughts and crosses game
// Author: Ben Longmire
// ben@blongmire.com

#include "userinput.h"

void player_turn(char player_name[], char piece, char board[3][3]);
void print_board(char board[3][3]);
void print_instructions(void);
bool detect_win(char board[3][3], char player_x[], char player_o[]);

int main(int argc, char const *argv[])
{
	char player_x[20] = {0};
	char player_o[20] = {0};
	char board[3][3] = {{0}};
	char piece = 'X';
	
	print_instructions();

	get_str("Player X name: ", player_x, 20);
	get_str("Player O name: ", player_o, 20);
	printf("\n");
	print_board(board);

	while(!detect_win(board, player_x, player_o))
	{
		switch(piece)
		{
			case 'X':
				player_turn(player_x, piece, board);
				piece = 'O';
				break;
			case 'O':
				player_turn(player_o, piece, board);
				piece = 'X';
				break;
		}
		
		print_board(board);
	}

	return 0;
}

void player_turn(char player_name[], char piece, char board[3][3])
{
	char turn[6];
	int x = 1, y = 1;
	char prompt[45] = {0};
	sprintf(prompt, "Player %s's turn (X,Y): ", player_name);
	while(1)
	{
		get_str(prompt, turn, 6);
		if(!isdigit(turn[0]) || turn[1] != ',' || !isdigit(turn[2]))
		{
			printf("Coordinate %s is invalid\n", turn);
			printf("Enter only numbers separated by a comma\n");
			printf("A coordinate like so: 1,1 or 3,3\n");
			continue;
		}
		
		sscanf(turn, "%d,%d", &x, &y);
		
		if(x < 1 || y < 1 || x > 3 || y > 3)
		{
			printf("Coordinate %d,%d is invalid\n", x, y);
			printf("Please enter only from 1,1 to 3,3\n");
			continue;
		}


		if(!board[y-1][x-1])
		{
			board[y-1][x-1] = piece;
			return;
		}
		else
		{
			printf("A piece is already in spot %d,%d\nChoose another\n", x, y);
		}
	}
}

void print_board(char board[3][3])
{
	printf("    1   2   3\n");
	printf("  +---+---+---+\n");
	for(int i = 0; i < 6; i++)
	{
		if(i % 2 > 0)
		{
			printf("  +---+---+---+\n");
			continue;
		}
		else
		{
			printf("%d | ", i / 2 + 1);
		}
		for(int j = 0; j < 3; j++)
		{
			if(board[i / 2][j])
			{
				printf("%c | ", board[i / 2][j]);	
			}
			else
			{
				printf("  | ");
			}
			
		}
		printf("\n");
	}
	printf("\n");
}

void print_instructions(void)
{
	printf("Welcome to Naughts and Crosses!\n\n");
	printf("INSTRUCTIONS:\n");
	printf("To make a move you enter in X and Y coordinates eg:\n1,1 \n3,3\n\n");
	printf("EXAMPLE:\nO in 1,1 and X in 3,3\n\n");

	printf("    1   2   3\n");
	printf("  +---+---+---+\n");
	printf("1 | O |   |   |\n");
	printf("  +---+---+---+\n");
	printf("2 |   |   |   |\n");
	printf("  +---+---+---+\n");
	printf("3 |   |   | X |\n");
	printf("  +---+---+---+\n");

	printf("\nTime to play...\n");
}

bool detect_win(char board[3][3], char player_x[], char player_o[])
{
	char winning_piece = 0;

	// Diagonal top left to bottom right
	if(board[0][0] && board[0][0] == board[1][1] && board[0][0] == board[2][2])
	{
		winning_piece = board[0][0];
		
	}
	// Diagonal top right to bottom left
	else if(board[2][0] && board[2][0] == board[1][1] && board[2][0] == board[0][2])
	{
		winning_piece = board[2][0];
	}
	else
	{
		for(int i = 0; i < 3; i++)
		{
			// Horizonal
			if(board[i][0] && board[i][0] == board[i][1] && board[i][0] == board[i][2])
			{
				winning_piece = board[i][0];
				break;
			}
			// Vertical
			if(board[0][i] && board[0][i] == board[1][i] && board[0][i] == board[2][i])
			{
				winning_piece = board[0][i];
				break;
			}
		}
	}
	
	switch(winning_piece)
	{
		case 'X':
			printf("%s WINS!\n", player_x);
			return true;
		case 'O':
			printf("%s WINS!\n", player_o);
			return true;
	}

	// Check each spot on the board, count the full spots
	int count = 0;
	for(int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if(board[i][j])
			{
				count++;
			}
			else
			{
				return false;
			}
		}
	}

	// If all spots are full, and there wasn't a win detected above, then it is a tie game
	if(count == 9)
	{
		printf("DRAW!\n");
		return true;
	}

	return false;
}