/**
 * User input functions
 * Author: Ben Longmire - ben@blongmire.com
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

void get_str(char *prompt, char *dest, int length);
void get_int(char *prompt, int *dest);
void get_char(char *prompt, char *dest);
bool is_str_int(char *input);
bool convert_str_int(int *dest, char *input);
